package logica.tests;

import static org.junit.Assert.*;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import logica.Grafo;

public class GrafoTest {
	
	private Grafo grafo;
	
	@Before
	public void crearGrafo() {
		this.grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 1);
	}
	
	@Test
	public void aristaExistenteTest() {
		assertTrue( grafo.existeArista(2, 3) );
	}

	@Test
	public void aristaOpuestaTest() {
		assertTrue( grafo.existeArista(3, 2) );
	}

	@Test
	public void aristaInexistenteTest() {
		assertFalse( grafo.existeArista(1, 4) );
	}

	@Test
	public void agregarAristaDosVecesTest() {
		grafo.agregarArista(2, 3, 1);
		assertTrue( grafo.existeArista(2, 3) );
	}

	@Test
	public void eliminarAristaExistenteTest() {
		grafo.borrarArista(2, 4); 
		assertFalse( grafo.existeArista(2, 4) );
	}

	@Test
	public void eliminarAristaInexistenteTest() {
		grafo.borrarArista(2, 4);
		assertFalse( grafo.existeArista(2, 4) );
	}
	
	@Test
	public void eliminarAristaDosVecesTest() {
		grafo.borrarArista(2, 4);
		grafo.borrarArista(2, 4);
		assertFalse( grafo.existeArista(2, 4) );
	}
	
	@Test
	public void vecinosCorrectos() {
		grafo.agregarArista(4, 3, 1);
		Set<Integer>vecinosA=grafo.getVecinos(2);
		Set<Integer>vecinosB=grafo.getVecinos(4);	
		assertTrue(Assert.vecinosIguales(vecinosA, vecinosB));
	}
	
	@Test
	public void vecinosIncorrectos() {
		grafo.agregarArista(4, 3, 1);
		Set<Integer>vecinosA=grafo.getVecinos(2);
		Set<Integer>vecinosB=grafo.getVecinos(0);	
		assertFalse(Assert.vecinosIguales(vecinosA, vecinosB));
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaVerticeIgual() {
		grafo.agregarArista(0,0,1);
	}

	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoSuperior() {
		grafo.agregarArista(5,0,1);
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaNegativa() {
		grafo.agregarArista(-1,0,1);
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void pesoInvalidoTest() {
		grafo.agregarArista(0, 1, 0);
	}


}
