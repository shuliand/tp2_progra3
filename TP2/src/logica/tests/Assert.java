package logica.tests;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import logica.Grafo;

public class Assert {

	// Verifica que sean iguales como conjuntos
	public static void arbolesIguales(Grafo grafoOriginal,Grafo grafoAComparar) {
		assertEquals(grafoAComparar.toString(),grafoOriginal.toString());
		assertEquals(grafoAComparar.cantidadVertices(),grafoOriginal.cantidadVertices());	
	}
	
	public static boolean vecinosIguales(Set<Integer> vecinosOriginal, Set<Integer> vecinosAComparar) {
		return (vecinosOriginal.equals(vecinosAComparar));
	}


}
