package logica;

import java.util.ArrayList;

public class ArbolGeneradorMinimo extends Grafo{
	
	
	public ArbolGeneradorMinimo(Grafo grafo,int vertices) {
		super(vertices);
		generarArbol(grafo);
	}
	
	private void generarArbol(Grafo grafo) {
		if(grafo==null) {
			throw new IllegalArgumentException("El grafo no puede ser null");
		}		
		boolean[] verticesMarcados=new boolean[grafo.cantidadVertices()];
		verticesMarcados[0]=true;	
		int i=0;
		while(i<grafo.cantidadVertices()-1) {		
				int[] verticeNuevo=aristaMenorPeso(grafo.getPesos(),verticesParaRecorrer(grafo,verticesMarcados));	
				super.agregarArista(verticeNuevo[0], verticeNuevo[1], verticeNuevo[2]);
				verticesMarcados[verticeNuevo[1]]=true;
				i++;
		}
		
	}
	
	private static int[] aristaMenorPeso(int[][] pesos,ArrayList<Integer> verticesParaRecorrer) {
		int arista=Integer.MAX_VALUE;
		int[] vertice=new int[3];//[0] vertice actual, [1] vertice destino, [2] peso
		int i=0,j=0;
		while(i<verticesParaRecorrer.size()) {			
			int filaActual=verticesParaRecorrer.get(i);	
			if(verticesParaRecorrer.contains(j)) {//no chequea un vertice/arista que ya posee
				j++;
			}else {					
				if (j >= pesos[0].length) {
					i++;
					j = 0;
				}else if(pesos[filaActual][j]<arista  && pesos[filaActual][j]!=0 ) { 
					
					arista=pesos[filaActual][j];
					vertice[0]=filaActual;
					vertice[1]=j;
					vertice[2]=arista;//peso arista		
				} else {
					j++;
				}
			}
		}		
		return vertice;
	}
	
	private static ArrayList<Integer> verticesParaRecorrer(Grafo grafo,boolean[] verticesMarcados) {
		ArrayList<Integer> ret=new ArrayList<>();
		
		for(int i=0;i<verticesMarcados.length;i++) {
			if(verticesMarcados[i]) {
				ret.add(i);
			}
		}
		return ret;		
	}
	
}
