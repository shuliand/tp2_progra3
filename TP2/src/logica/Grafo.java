package logica;

import java.util.HashSet;
import java.util.Set;

public class Grafo {
	
	private boolean[][] A; //Matriz de adyacencia
	private int[][] pesos;
	private int vertices;
	
	public Grafo(int vertices){
		this.A=new boolean[vertices][vertices];
		this.pesos=new int[vertices][vertices];
		this.vertices=vertices;	
	}
	
	public void agregarArista(int i, int j,int pesoArista) {
		verificarVertisesIgual(i, j);
		verificarVertice(i);
		verificarVertice(j);	
		verificarPeso(pesoArista);
		
		A[i][j]=true;
		A[j][i]=true;
		pesos[i][j]=pesoArista;
		pesos[j][i]=pesoArista;			
	}
	
	public void borrarArista(int i,int j) {
		verificarVertisesIgual(i, j);
		verificarVertice(i);
		verificarVertice(j);
	
		A[i][j]=false;
		A[j][i]=false;
		pesos[i][j]=0;
		pesos[j][i]=0;
	}
	
	public boolean existeArista(int i,int j) {
		verificarVertisesIgual(i, j);
		verificarVertice(i);
		verificarVertice(j);
		return A[i][j];
	}
	
	private void verificarVertisesIgual(int i,int j) {
		if(i==j) {
			throw new IllegalArgumentException("Los vertices no pueden ser iguales: "+i+"="+j);
		}	
	}
	
	private void verificarVertice(int i) {
		if(i<0) {
			throw new IllegalArgumentException("El vertice no puede ser negativo");
		}
		if(i>=vertices) {
			throw new IllegalArgumentException("El vertice no puede ser mayor que la cantidad de vertices del grafo");
		}
	}
	
	private void verificarPeso(int peso) {
		if(peso<0) {
			throw new IllegalArgumentException("El peso no debe ser menor a cero: ");
		}
	}
	
	public boolean sonVecinos(int i, int j) {
      return A[i][j];
    }
		
	public Set<Integer> getVecinos(int i) {
		verificarVertice(i);
		Set<Integer> vecinos=new HashSet<Integer>();
		for(int j = 0; j < vertices; ++j) if( i != j ) {
			if( this.existeArista(i,j) )
				vecinos.add(j);
		}
		return vecinos;
	}

	public int cantidadVertices() {
		int cantidadVertices= A.length;
		return cantidadVertices;
	}
	
	public int[][] getPesos() {
		return pesos;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<this.A.length;i++) { //fila
			for(int j=0;j<this.A[0].length;j++) {//columna
				if(A[i][j]) {
					sb.append("1  ");
				}else { 
					sb.append("0  ");
				}			
			}
			sb.append("\n");
		}
		return sb.toString();
	}
		
}
