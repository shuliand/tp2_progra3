package logica;

public class Persona {
	
	private String nombre;
	private int interesDeportes;
	private int interesMusica;
	private int interesNoticias;
	private int interesCiencia;
	
	public Persona (String nombre,int interesDeportes,
			int interesMusica,int interesNoticias,int interesCiencia) {
		this.nombre=nombre;
		
		if(verificarValorInteres(interesDeportes)) {
			this.interesDeportes=interesDeportes;
		}else {
			throw new RuntimeException("Los valores deben estar entre 1 y 5");
		}
		
		if(verificarValorInteres(interesMusica)) {
			this.interesMusica=interesMusica;
		}else {
			throw new RuntimeException("Los valores deben estar entre 1 y 5");
		}
		
		if(verificarValorInteres(interesNoticias)) {
			this.interesNoticias=interesNoticias;
		}else {
			throw new RuntimeException("Los valores deben estar entre 1 y 5");
		}
		
		if(verificarValorInteres(interesCiencia)) {
			this.interesCiencia=interesCiencia;
		}else {
			throw new RuntimeException("Los valores deben estar entre 1 y 5");
		}
	
	}

	private boolean verificarValorInteres(int valorInteres) {
		if (valorInteres>=1 && valorInteres<=5) {
			return true;
		}else {
			return false;
		}
	}

	public String getNombre() {
		return nombre;
	}

	public int getInteresDeportes() {
		return interesDeportes;
	}

	public int getInteresMusica() {
		return interesMusica;
	}

	public int getInteresNoticias() {
		return interesNoticias;
	}

	public int getInteresCiencia() {
		return interesCiencia;
	}
	
	@Override
	public String toString() {
		return nombre+": ( Interes Deportes:"+interesDeportes+","
				+"Interes Musica:"+interesMusica+","
				+"Interes Noticias:"+interesNoticias+","
				+"Interes Ciencia:"+interesCiencia+" ).\n";
	}


	
}
