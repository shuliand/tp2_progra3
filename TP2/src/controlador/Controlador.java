package controlador;

import java.util.ArrayList;

import logica.ArbolGeneradorMinimo;
import logica.Grafo;
import logica.Persona;


public class Controlador {

	private ArrayList<Persona> personas;
	
	private String [][] matrizDePersonas;
	private String [][] matrizDeGrupos;
	private ArbolGeneradorMinimo arbol;
	private Grafo grafo;

	
	public Controlador () {
		this.personas= new ArrayList<Persona>();
//		Para probar con personas ya registradas
//		agregarPersonas();     
	}
	
	public void agregarPersona(String nombre,int interesDeportes,
			int interesMusica,int interesNoticias,int interesCiencia) {
		Persona p = new Persona(nombre, interesDeportes, interesMusica, interesNoticias, interesCiencia);
		personas.add(p);
	}
	
	public void generarAGM() {
		grafo= generarGrafo(personas.size());
		arbol=new ArbolGeneradorMinimo(grafo,personas.size());
	}
		
	private Grafo generarGrafo(int cantidadVertices) {
		Grafo grafo= new Grafo(cantidadVertices);		
		for (int i = 0; i < cantidadVertices; i++) {
            for (int j = i + 1; j < cantidadVertices; j++) {
            	int peso = similaridad(personas.get(i),personas.get(j));
            	grafo.agregarArista(i, j, peso);
            }
		}
		return grafo;
	}
	
	private int similaridad(Persona p1, Persona p2) {
		return Math.abs(p1.getInteresDeportes() - p2.getInteresDeportes())+
				Math.abs(p1.getInteresMusica() - p2.getInteresMusica())+
				Math.abs(p1.getInteresNoticias() - p2.getInteresNoticias())+
				Math.abs(p1.getInteresCiencia() - p2.getInteresCiencia());	
	}
	
	public void generarComponentesConexas() {
		eliminarAristaMayor();	
	}
	
	private void eliminarAristaMayor() {
		int pesoMaximo=0;
		int fila=0;
		int columna=0;
		int[][]pesos= arbol.getPesos();
		
		for (int i=0;i<pesos.length;i++) {
			for (int j=0;j<pesos[0].length;j++) {
				if(pesos[i][j]>pesoMaximo) {
					pesoMaximo=pesos[i][j];
					fila=i;
					columna=j;
				}
			}
		}
		arbol.borrarArista(fila, columna);
	}
	
	
	public ArbolGeneradorMinimo getArbol() {
		return arbol;
	}
		
	public String getPersonas() {
		return personas.toString();		
	}
	
	public String [][] matrizDePersonas(){
		matrizDePersonas = new String [personas.size()][5];	
		for (int i=0; i<personas.size(); i++) {
			matrizDePersonas[i][0]= personas.get(i).getNombre();
			matrizDePersonas[i][1]=personas.get(i).getInteresDeportes()+"";
			matrizDePersonas[i][2]=personas.get(i).getInteresMusica()+"";
			matrizDePersonas[i][3]=personas.get(i).getInteresNoticias()+"";
			matrizDePersonas[i][4]=personas.get(i).getInteresCiencia()+"";	
		}
		return matrizDePersonas;	
	}
	
	public String [][] generarMatrizDeGrupos(){	
		matrizDeGrupos = new String [personas.size()][2];
		matrizDeGrupos[0][0]= personas.get(0).getNombre();
		matrizDeGrupos[0][1]= "1";
		
		for (int i=1; i<personas.size(); i++) {
			
			if(arbol.sonVecinos(0, i)) {
				matrizDeGrupos[i][0]= personas.get(i).getNombre();
				matrizDeGrupos[i][1]= "1";
			}else {
				matrizDeGrupos[i][0]= personas.get(i).getNombre();
				matrizDeGrupos[i][1]= "2";
			}
		}
		return matrizDeGrupos;
	}
	
	public String matrizGruposToString() {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<matrizDeGrupos.length;i++) { //fila
			for(int j=0;j<matrizDeGrupos[0].length;j++) {//columna
				 sb.append(matrizDeGrupos[i][j]+"   ");			
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	
	public boolean hayPersonas() {
		if (personas.size()==0) {
			return false;
		}else {
			return true;
		}
	}
	


	@SuppressWarnings("unused")
	private void agregarPersonas() {
		 	personas.add(new Persona("Franco", 1, 2, 5, 3));
	        personas.add(new Persona("Jose", 3, 2, 1, 4));
	        personas.add(new Persona("Sandra", 2, 2, 2, 2));
	        personas.add(new Persona("Milagros", 1, 4, 4, 4));
	        personas.add(new Persona("Pedro", 2, 5, 1, 2));
	        personas.add(new Persona("Pepe", 2, 1, 2, 5));
	        personas.add(new Persona("Emilia", 1, 2, 3, 2));
	        personas.add(new Persona("Juan", 2, 4, 3, 2));
	        personas.add(new Persona("Camila", 2, 2, 2, 2));
	        personas.add(new Persona("Ludmila", 2, 3, 4, 2));
	        personas.add(new Persona("German", 2, 2, 2, 5));
	        personas.add(new Persona("Agustin", 2, 5, 2, 2));
	        personas.add(new Persona("Roberto", 2, 3, 2, 4));
	        personas.add(new Persona("Liliana", 2, 2, 3, 4));
	        personas.add(new Persona("Elsa", 5, 2, 2, 2));
	        personas.add(new Persona("Federico", 2, 2, 2, 2));
	        personas.add(new Persona("Cristina", 2, 2, 2, 2));
	        personas.add(new Persona("Carla", 2, 2, 2, 2));
	        personas.add(new Persona("Carlos", 2, 2, 2, 2));
	        personas.add(new Persona("Fernando", 5, 2, 3, 2));
	        personas.add(new Persona("Santiago", 2, 3, 2, 2));
	        personas.add(new Persona("Lorenzo", 2, 2, 2, 2));
	        personas.add(new Persona("Ciro", 1, 2, 5, 2));
	        personas.add(new Persona("Daiana", 4, 2, 2, 2));
	        personas.add(new Persona("Lourdes", 3, 2, 2, 2));
	        personas.add(new Persona("Tomas", 5, 2, 2, 2));
	        personas.add(new Persona("Lucas", 5, 2, 2, 2));
	        personas.add(new Persona("Javier", 4, 2, 2, 2));
	        personas.add(new Persona("Geronimo", 1, 2, 2, 2));
	        personas.add(new Persona("Bautista", 1, 2, 2, 2));
	        personas.add(new Persona("Leandro", 2, 4, 2, 2));
	        personas.add(new Persona("Nahuel", 3, 2, 5, 2));
	        personas.add(new Persona("Nicolas", 5, 2, 4, 5));
	        personas.add(new Persona("Esteban", 4, 3, 2, 2));
	        personas.add(new Persona("Lautaro", 1, 2, 2, 2));
	        personas.add(new Persona("Enzo", 3, 2, 2, 2));
	        personas.add(new Persona("Benjamin", 3, 5, 2, 2));
	        personas.add(new Persona("Ezequiel", 5, 2, 2, 2));
	        personas.add(new Persona("Patricio", 4, 2, 4, 2));
	        personas.add(new Persona("Brenda", 1, 2, 2, 2));
	        personas.add(new Persona("Lara", 4, 2, 2, 2));
	        personas.add(new Persona("Julieta", 2, 3, 2, 2));
	        personas.add(new Persona("Juana", 1, 2, 2, 2));
	        personas.add(new Persona("Jazmin", 2, 1, 2, 2));
	        personas.add(new Persona("Alfredo", 3, 2, 2, 2));
	        personas.add(new Persona("Mario", 1, 4, 2, 5));
	        personas.add(new Persona("Martin", 3, 4, 3, 4));
	        personas.add(new Persona("Miguel", 2, 4, 4, 1));
	        personas.add(new Persona("Juliana", 3, 2, 4, 2));
	        personas.add(new Persona("Julian", 1, 5, 5, 2));
	        personas.add(new Persona("David", 3, 2, 1, 1));
	        personas.add(new Persona("Renzo", 3, 3, 2, 2));
	        personas.add(new Persona("Gaston", 5, 5, 1, 5));
	        personas.add(new Persona("Lucia", 2, 3, 2, 1));
	        personas.add(new Persona("Micaela", 5, 5, 2, 1));
	        personas.add(new Persona("Evelin", 2, 3, 3, 2));
	        personas.add(new Persona("Azul", 2, 5, 5, 2));
	        personas.add(new Persona("Ariana", 1, 2, 2, 3));
	        personas.add(new Persona("Cintia", 2, 2, 5, 5));       
	}

}