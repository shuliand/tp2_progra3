package visual;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class TablaDeGrupos extends JPanel {
	
	
	private JPanel panelGrupo1;
	private JPanel panelGrupo2;
	private ArrayList<String> nombresGrupo1;
	private ArrayList<String> nombresGrupo2;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;


	/**
	 * Create the panel.
	 */
	public TablaDeGrupos(String [][] matrizDePersonas) {	
		nombresGrupo1 = new ArrayList<String>();
		nombresGrupo2 = new ArrayList<String>();
		setBackground(new Color(192, 192, 192));
		setBounds(0, 0, 800, 500);
		setLayout(null);
		
		
		for (int i=0;i<matrizDePersonas.length;i++) {
			if (matrizDePersonas[i][1].equals("1")) {
				nombresGrupo1.add(matrizDePersonas[i][0]);
			}else {
				nombresGrupo2.add(matrizDePersonas[i][0]);
			}
		}	
		
		generarPanelGrupo1();
		generarPanelGrupo2();
		
	}
	

	private void generarPanelGrupo1() {
		panelGrupo1 = new JPanel();	
		panelGrupo1.setLayout(null);
		panelGrupo1.setPreferredSize(new Dimension(379,(nombresGrupo1.size()+2)*30));
		
		scrollPane1 = new JScrollPane(panelGrupo1);
		scrollPane1.setBounds(5, 23, 379, 477);
		add(scrollPane1);	
		
		JLabel labelGrupo1 = new JLabel("GRUPO 1");
		labelGrupo1.setBounds(0, 11, 400, 14);
		add(labelGrupo1);
		labelGrupo1.setHorizontalAlignment(SwingConstants.CENTER);
		
		int y=30;
		for (int i=0;i<nombresGrupo1.size();i++) {
			panelGrupo1.add(getLabel(nombresGrupo1.get(i),0, y, 400, 14));
			y+=30;
		}
	}
	
	private void generarPanelGrupo2() {
		panelGrupo2 = new JPanel();
		panelGrupo2.setLayout(null);
		panelGrupo2.setPreferredSize(new Dimension(391,(nombresGrupo2.size()+2)*30));
		
		scrollPane2 = new JScrollPane(panelGrupo2);
		scrollPane2.setBounds(390, 23, 391, 477);
		add(scrollPane2);	
			
		JLabel labelGrupo2 = new JLabel("GRUPO 2");
		labelGrupo2.setBounds(399, 11, 401, 14);
		add(labelGrupo2);
		labelGrupo2.setHorizontalAlignment(SwingConstants.CENTER);
		
		int y=30;
		for (int i=0;i<nombresGrupo2.size();i++) {
			panelGrupo2.add(getLabel(nombresGrupo2.get(i),0, y, 400, 14));
			y+=30;
		}
	}
	
	private JLabel getLabel(String text, int x, int y, int h, int w) {
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBounds(x,y,h,w);
        return label;
    }
}
