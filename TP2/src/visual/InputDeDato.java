package visual;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class InputDeDato extends JPanel {
	
    protected boolean mostrarMensajeDeError = false;
    protected JTextField textField;
    protected JLabel label;
    protected JLabel mensajeDeError;
    
    public InputDeDato(String labelText, int posicionY) {
        super();
        setLayout(new GridLayout(2,1));
        mensajeDeError  = new JLabel("");
        mensajeDeError.setFont(new Font("Arial", Font.PLAIN, 20));
        mensajeDeError.setHorizontalAlignment(SwingConstants.CENTER);
        mensajeDeError.setForeground(Color.red);
        JPanel inputPanel = new JPanel();
        
        textField =new  JTextField();
        textField.setColumns(15);
        label = new JLabel(labelText);
        textField.setFont(new Font("Arial", Font.PLAIN, 20));
        textField.setPreferredSize(new Dimension(textField.getWidth(), 30));
        label.setFont(new Font("Arial", Font.PLAIN, 25));
        inputPanel.add(label);
        inputPanel.add(textField);
        add(inputPanel);

        add(mensajeDeError);
        setBounds(100, posicionY, 600, 70);
    }

    public void mostrarMensajeDeError(String mensaje) {
        this.mensajeDeError.setText(mensaje);
        // this.mensajeDeError.repaint();
    }
    
    public void ocultarMensajeDeError() {
        this.mensajeDeError.setText("");
    }

    public String getText() {
        return this.textField.getText();
    }

	public void setTextField(String text) { 
		this.textField.setText(text);
	}
   
   
    
}
