package visual;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class FormularioPersona extends JPanel {
	
	InputDeDato textFieldNombre;
	public InputDeDato textFieldDeportes;
	public InputDeDato textFieldMusica;
	public InputDeDato textFieldNoticias;
	public InputDeDato textFieldCiencia;
	
	 
	/**
	 * Create the panel.
	 */
	public FormularioPersona() {
		
		setBounds(0, 0, 800, 400);
		setLayout(null);
		setOpaque(true);
		
		generarTextFields();
	}
	
	
	private void generarTextFields() {
		this.textFieldNombre = new InputDeDato("Nombre:  ", 29);
		this.textFieldDeportes = new InputDeDato("Interes en Deportes:  ", 99);
		this.textFieldMusica = new InputDeDato("Interes en Musica:  ", 169);
		this.textFieldNoticias = new InputDeDato("Interes en Noticias:  ", 249);
		this.textFieldCiencia = new InputDeDato("Interes en Ciencia:  ",329);

		add(textFieldNombre);
		add(textFieldDeportes);
		add(textFieldMusica);
		add(textFieldNoticias);
		add(textFieldCiencia);
	}
	
	public void limpiarTextFields() {
		textFieldNombre.setTextField("");
		textFieldDeportes.setTextField("");
		textFieldMusica.setTextField("");
		textFieldNoticias.setTextField("");
		textFieldCiencia.setTextField("");
	}
	
	
	public boolean camposValidos() {
		return verificarCampos();
	}
	
	private boolean  verificarCampos() {
		ocultarMensajesDeError();
		boolean camposValidos = true;
		camposValidos = camposValidos & verificarNombre(textFieldNombre);
		camposValidos= camposValidos & verificarInteresField(textFieldDeportes);
		camposValidos= camposValidos & verificarInteresField(textFieldMusica);
		camposValidos= camposValidos & verificarInteresField(textFieldNoticias);
		camposValidos= camposValidos & verificarInteresField(textFieldCiencia);
		return camposValidos;
	}
	
	private boolean verificarNombre(InputDeDato input) {
		try {
			verificarCampoVacio(input.getText());
		} catch (Exception e) {
			input.mostrarMensajeDeError(e.getMessage());
			return false;
		}
		return true;
	}
	private boolean verificarInteresField(InputDeDato input) {
		String contenido  = input.textField.getText();
		
		try {
			verificarCampoVacio(contenido);

			int numero = convertirNumero(contenido);
			if (numero<1 || numero > 5 ) {
				throw new RuntimeException("El valor debe estar entre 1 y 5");	
			}
		} catch (Exception e) {
			input.mostrarMensajeDeError(e.getMessage());
			return  false;
		} 
		return true;
	}

	private int convertirNumero (String contenido) {
		int test;
			try {
				test =  Integer.parseInt(contenido);
			} catch (Exception e) {
				throw new RuntimeException("Valor introducido invalido");
			}
		return test;
	}
	
	private void verificarCampoVacio(String contenido){
		if(contenido.isEmpty()){
			throw new RuntimeException("El campo no puede estar vacio");
		}
	}

	private void ocultarMensajesDeError() {
		textFieldNombre.ocultarMensajeDeError();
		textFieldDeportes.ocultarMensajeDeError();
		textFieldMusica.ocultarMensajeDeError();
		textFieldNoticias.ocultarMensajeDeError();
		textFieldCiencia.ocultarMensajeDeError();
		repaint();
	}


}
