package visual;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import controlador.Controlador;


public class MainForm {

	private JFrame frame;	
	private Controlador controlador;
	private FormularioPersona formularioPersona;
	private InterfazGrupo interfazGrupo;
	private JPanel mensajeListaVacia;
	private TablaDeGrupos tablaDeGrupos;
	private JPanel interfazActual;

	
	private JButton buttonGuardar;
	private JButton buttonVisualizar;
	private JButton buttonEjecutar;
	private JButton buttonVolver;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
						window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		controlador = new Controlador();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.white);
		frame.setLocationRelativeTo(null); // centro la ventana
			
		generarInterfazInicial();		
	}
	
	private void generarInterfazInicial() {
		formularioPersona = new FormularioPersona();
		frame.getContentPane().add(formularioPersona);
		interfazActual=formularioPersona;
		actualizar();
		generarBotonGuardar();
		generarBotonVisualizar();
		generarBotonEjecutar();
	}
	
	private void generarBotonGuardar() {
		buttonGuardar = new JButton("Guardar persona");
		buttonGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				if(formularioPersona.camposValidos()){
					crearPersona();
					formularioPersona.limpiarTextFields();
				   }		
			}
		});
		buttonGuardar.setBounds(89, 459, 144, 23);
		frame.getContentPane().add(buttonGuardar);
	}
	
	private void generarBotonVisualizar() {
		buttonVisualizar = new JButton("Visualizar grupo");
		buttonVisualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generarInterfazGrupo();
			}
		});
		buttonVisualizar.setBounds(319, 459, 144, 23);
		frame.getContentPane().add(buttonVisualizar);
	}
	
	private void generarInterfazGrupo() { 
		if(controlador.hayPersonas()) {
			interfazGrupo = new InterfazGrupo(controlador.matrizDePersonas());
			frame.getContentPane().add(interfazGrupo);
			interfazActual=interfazGrupo;
		}else {
			generarMensajeListaVacia();	
		}
		actualizar();
	}
	
	private void generarMensajeListaVacia() {
		eliminarBotones();
		frame.getContentPane().remove(interfazActual);
		generarBotonVolver();
		mensajeListaVacia = new JPanel();
		mensajeListaVacia.setBounds(0, 0, 800, 400);
		mensajeListaVacia.setLayout(null);
		frame.getContentPane().add(mensajeListaVacia);
		interfazActual=mensajeListaVacia;
		JLabel labelMensaje = new JLabel("Aún no se ha generado un grupo");
		labelMensaje.setHorizontalTextPosition(SwingConstants.CENTER);
		labelMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		labelMensaje.setBounds(10, 197, 764, 87);
		mensajeListaVacia.add(labelMensaje);
	}
	
	private void generarBotonVolver() {
		buttonVolver = new JButton("Volver");
		buttonVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().remove(interfazActual);
				frame.getContentPane().remove(buttonVolver);
				actualizar();
				generarInterfazInicial();	
			}
		});
		buttonVolver.setBounds(319, 510, 144, 23);
		frame.getContentPane().add(buttonVolver);
	}
	
	
	private void actualizar() {
		frame.revalidate();	
		frame.repaint();
	}
	
	private void eliminarBotones() {
		frame.remove(buttonGuardar);
		frame.remove(buttonVisualizar);
		frame.remove(buttonEjecutar);
	}
		
	private void generarBotonEjecutar() {
		buttonEjecutar = new JButton("Ejecutar ");
		buttonEjecutar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(controlador.hayPersonas()) {
					controlador.generarAGM();
					controlador.generarComponentesConexas();
					mostrarGruposGenerados();
				}else {
					generarMensajeListaVacia();
					actualizar();
				}
				
			}
		});
		buttonEjecutar.setBounds(549, 459, 144, 23);
		frame.getContentPane().add(buttonEjecutar);		
	}
	
	private void mostrarGruposGenerados() {
		eliminarBotones();
		frame.getContentPane().remove(formularioPersona);
		generarBotonVolver();
		if(controlador.hayPersonas()) {
			tablaDeGrupos = new TablaDeGrupos(controlador.generarMatrizDeGrupos());
			frame.getContentPane().add(tablaDeGrupos);
			interfazActual=tablaDeGrupos;
		}else {
			generarMensajeListaVacia();	
		}
		actualizar();
	}
		
	private void crearPersona( ) {
		String nombre = this.formularioPersona.textFieldNombre.getText();
		int deportes = Integer.parseInt(this.formularioPersona.textFieldDeportes.getText());
		int musica = Integer.parseInt(this.formularioPersona.textFieldMusica.getText());
		int noticias = Integer.parseInt(this.formularioPersona.textFieldNoticias.getText());
		int ciencias = Integer.parseInt(this.formularioPersona.textFieldCiencia.getText());
		controlador.agregarPersona(nombre, deportes, musica, noticias, ciencias);
		System.out.println(controlador.getPersonas());
	}
}
