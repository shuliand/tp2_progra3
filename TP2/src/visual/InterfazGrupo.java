package visual;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Dimension;


@SuppressWarnings("serial")
public class InterfazGrupo extends JPanel {

	private String [][] matrizDePersonas;
	private JPanel panelPrincipal;
	private JScrollPane scrollPane;
	/**
	 * Create the panel.
	 */
	public InterfazGrupo(String [][] matrizDePersonas) {
		this.matrizDePersonas=matrizDePersonas;	
		setBackground(new Color(192, 192, 192));
		setBounds(0, 0, 800, 500);
		setLayout(null);	
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(new Color(255,255,255));
		panelPrincipal.setLayout(null);
		panelPrincipal.setPreferredSize(new Dimension(666,(matrizTamanio()+2)*30));	
		scrollPane = new JScrollPane(panelPrincipal);
		scrollPane.setBounds(66, 38, 666, 410);
		add(scrollPane);
			
		generarTitulos();
		generarTabla();
					  
	}
	
	private void generarTitulos(){
		JLabel labelNombre = new JLabel("Nombre");
		labelNombre.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		labelNombre.setHorizontalAlignment(SwingConstants.CENTER);
		labelNombre.setBounds(0, 0, 190, 41);
		panelPrincipal.add(labelNombre);
		
		JLabel labelDeportes = new JLabel("Interes en Deportes");
		labelDeportes.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		labelDeportes.setHorizontalAlignment(SwingConstants.CENTER);
		labelDeportes.setBounds(190, 0, 119, 41);
		panelPrincipal.add(labelDeportes);
		
		JLabel labelMusica = new JLabel("Interes en Música");
		labelMusica.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		labelMusica.setHorizontalAlignment(SwingConstants.CENTER);
		labelMusica.setBounds(309, 0, 119, 41);
		panelPrincipal.add(labelMusica);
		
		JLabel labelNoticias = new JLabel("Interes en Noticias");
		labelNoticias.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		labelNoticias.setHorizontalAlignment(SwingConstants.CENTER);
		labelNoticias.setBounds(428, 0, 119, 41);
		panelPrincipal.add(labelNoticias);
		
		JLabel labelCiencia = new JLabel("Interes en Ciencia");
		labelCiencia.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		labelCiencia.setHorizontalAlignment(SwingConstants.CENTER);
		labelCiencia.setBounds(547, 0, 119, 41);
		panelPrincipal.add(labelCiencia);
	}
	
	private void generarTabla() {
		int y=30;
		for (int i=0;i<matrizTamanio();i++) {
			panelPrincipal.add(getLabel(matrizDePersonas[i][0],0, y, 190, 41));
			panelPrincipal.add(getLabel(matrizDePersonas[i][1],190, y, 119, 41));
			panelPrincipal.add(getLabel(matrizDePersonas[i][2],309, y, 119, 41));
			panelPrincipal.add(getLabel(matrizDePersonas[i][3],428, y, 119, 41));
			panelPrincipal.add(getLabel(matrizDePersonas[i][4],547, y, 119, 41));
		    
		    y+=30;
		}
	}

	private int matrizTamanio() {
		return matrizDePersonas.length;
	}
	
	
	private JLabel getLabel(String text, int x, int y, int h, int w) {
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBounds(x,y,h,w);
        return label;
    }
}
